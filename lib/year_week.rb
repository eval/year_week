require 'values'
require 'active_record'

class YearWeek < Value.new(:year, :week)
  include Comparable

  def <=>(other)
    to_s <=> other.to_s
  end

  def to_s
    "#{@year}-#{@week}"
  end

  def include?(other)
    to_range.include?(other.to_date)
  end

  def self.for_date(date)
    new(date.cwyear, date.cweek)
  end

  def self.current
    for_date(Date.current)
  end

  def to_range
    (self.begin..self.end)
  end

  def begin
    DateTime.commercial(@year, @week)
  end

  def end
    DateTime.commercial(@year, @week, 7, 23, 59, 59)
  end

  def succ
    self.class.for_date(self.begin.next_week)
  end

  def pred
    self.class.for_date(self.begin.prev_day)
  end

  def lt(att)
    att.gt(self.end)
  end

  def lteq(att)
    pred.lt(att)
  end

  def gt(att)
    att.lt(self.begin)
  end

  def gteq(att)
    succ.gt(att)
  end
end

# TODO: move this to railtie
ActiveRecord::PredicateBuilder.register_handler(YearWeek, ->(attribute, value) { attribute.between(value.to_range) })
