# YearWeek

Makes working with weeks more fun.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'year_week'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install year_week

## Usage


```ruby
# Create:
> YearWeek.new(2016, 1)
=> #<YearWeek year=2016, week=1>

> YearWeek.for_date(Date.parse('2016-1-4'))
=> #<YearWeek year=2016, week=1>

# The current week:
> YearWeek.current
=> #<YearWeek year=2016, week=9>

# 'Change':
# Weeks are immutable. By using `#with` you create a new week:
> YearWeek.current.with(week: 9)
=> #<YearWeek year=2016, week=9>

# next and previous:
> YearWeek.new(2016, 1).succ == YearWeek.new(2016, 3).pred
=> true

# Comparing
# Equality is based on value:
> YearWeek.current.with(week: 8) == YearWeek.new(2016, 8)
=> true

> YearWeek.current.with(week: 8) < YearWeek.current.with(week: 9)
=> true

# Check for inclusion:
> YearWeek.new(2016, 1).include?(Date.parse('2016-1-4'))
=> true
```

### ActiveRecord/Arel

Given a `User`-class and a week `@week` you can do the following:

```ruby
# All users created during the week:
User.where(created_at: @week)

# all users created before the week:
user_table = User.arel_table
User.where(@week.gt(user_table[:created_at]))
# ...including users created during week
User.where(@week.gteq(user_table[:created_at]))

# similar to get all users created after a week:
User.where(@week.lt(user_table[:created_at]))
# ...including users created during week
User.where(@week.lteq(user_table[:created_at]))
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## TODO

- [ ] range should have datetimes
- [ ] use ISO8601 for `#to_s`
- [ ] make it work without AR
- [ ] add `YearWeek()`
- [ ] range of weeks `YearWeek.new(2016, 1)...YearWeek.new(2016, 3)`

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

