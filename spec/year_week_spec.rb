require 'spec_helper'

describe YearWeek do
  def rails_setup!
    ActiveRecord::Base.establish_connection(adapter: 'sqlite3', database: ':memory:')
    #ActiveRecord::Base.logger = Logger.new(STDOUT) # nice for debugging

    ActiveRecord::Schema.define do
      create_table :users, force: true do |t|
        t.string :name
        t.timestamps null: false
      end
    end
  end

  def user_class
    @user_class ||= begin
      rails_setup!
      Class.new(ActiveRecord::Base) do
        self.table_name = 'users'
      end
    end
  end

  def week(week, year: 2016)
    YearWeek.new(year, week)
  end

  def date(s)
    Date.parse(s)
  end

  def user_created(datetime)
    user_class.create(created_at: datetime)
  end

  let(:week1) { week(1, year: 2016) }
  let(:week2) { week(2, year: 2016) }

  describe 'Arel-between' do
    it 'yields user created at lower edge of week' do
      user = user_created(week1.begin)
      expect(user_class.where(created_at: week1)).to include user
    end

    it 'yields user created at higher edge of week' do
      user = user_created(week1.end - 1.second)
      expect(user_class.where(created_at: week1)).to include user
    end
  end

  describe '#gt' do
    def find_created_before(week)
      utable = user_class.arel_table
      user_class.where(week.gt(utable[:created_at]))
    end

    it 'finds user *just* created in week' do
      user = user_created(week1.end - 1.second)

      expect(find_created_before(week2)).to include user
    end

    it 'does not find user created in next week' do
      user = user_created(week2.begin)

      expect(find_created_before(week2)).to_not include user
    end
  end

  describe '#gteq' do
    def find_created_before_ending_of(week)
      utable = user_class.arel_table
      user_class.where(week.gteq(utable[:created_at]))
    end

    it 'finds user *just* created in week' do
      user = user_created(week1.end - 1.second)

      expect(find_created_before_ending_of(week1)).to include user
    end

    it 'does not find user created in next week' do
      user = user_created(week2.begin)

      expect(find_created_before_ending_of(week1)).to_not include user
    end
  end

  describe '#lt' do
    def find_created_after(week)
      utable = user_class.arel_table
      user_class.where(week.lt(utable[:created_at]))
    end

    it 'finds user *just* created in next week' do
      user = user_created(week2.begin)

      expect(find_created_after(week1)).to include user
    end

    it 'does not find user created in next week' do
      user = user_created(week1.end - 1.second)

      expect(find_created_after(week1)).to_not include user
    end
  end

  describe '#lteq' do
    def find_created_after_start_of(week)
      utable = user_class.arel_table
      user_class.where(week.lteq(utable[:created_at]))
    end

    it 'finds user *just* created in week' do
      user = user_created(week1.begin)

      expect(find_created_after_start_of(week1)).to include user
    end

    it 'does not find user created in previous week' do
      user = user_created(week1.begin - 1.second)

      expect(find_created_after_start_of(week1)).to_not include user
    end
  end

  describe '#pred' do
    it 'yields the previous week' do
      expect(week2.pred).to eq week1
    end
  end

  describe '#succ' do
    it 'yields the next week' do
      expect(week1.succ).to eq week2
    end
  end

  describe 'comparing' do
    specify '<' do
      expect(week(12)).to be < week(13)
      expect(week(12, year: 2015)).to be < week(12)
    end

    specify '>' do
      expect(week(13)).to be > week(12)
      expect(week(13)).to be >= week(13)
      expect(week(13)).to be > week(12, year: 2015)
    end

    specify 'equality' do
      expect(week(13)).to be === week(13)
      expect(week(12)).to be == week(12)
      expect(week(12)).to eql(week(12))
    end

    it 'sorts correctly' do
      expect([week(12), week(11)].sort).to eq [week(11), week(12)]
    end
  end

  describe 'include?' do
    it 'yields true for date within that week' do
      # week 12: Mon, 21 Mar 2016...Mon, 28 Mar 2016
      expect(week(12)).to include(date('2016-3-21'))
      expect(week(12)).to include(date('2016-3-22'))

      expect(week(12)).to_not include(date('2016-3-28'))
    end

    it 'yields true for time within that week' do
      # week 12: Mon, 21 Mar 2016...Mon, 28 Mar 2016
      expect(week(12)).to include(date('2016-3-21 0:00'))

      expect(week(12)).to include(date('2016-3-27 19:00'))
      expect(week(12)).to_not include(date('2016-3-28 0:00'))
    end
  end

  describe '#succ' do
    it 'yields next week' do
      expect(week(12).succ).to eq week(13)
      expect(week(52, year: 2008).succ).to eq week(1, year: 2009)
    end
  end

  describe '::for_date' do
    def for_date(s)
      described_class.for_date(date(s))
    end

    it 'yields instance for date' do
      expect(for_date('2016-1-4')).to eq week(1)
      expect(for_date('2016-1-5')).to eq week(1)
      expect(for_date('2008-12-29')).to eq week(1, year: 2009)
    end
  end

  describe '::current' do
    it 'calls ::for_date with Date.current' do
      expect(Date).to receive(:current)
      expect(described_class).to receive(:for_date)

      YearWeek.current
    end
  end
end
